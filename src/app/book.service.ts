import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class BookService {

    constructor(private http: HttpClient) { }


    getBooks() {
        const uri = 'http://altkom.test/api/book';
        return this
            .http
            .get(uri)
            .map(res => {
                return res;
            });
    }
    showBook(id) {
        const uri = 'http://altkom.test/api/book/' + id;
        return this
            .http
            .get(uri)
            .map(res => {
                return res;
            });
    }
    searchBook(name) {
        const uri = 'http://altkom.test/api/book/search';
        return this
            .http
            .get(uri)
            .map(res => {
                return res;
            });
    }
    addBook(isbn,title,author,type,pages,release_date) {
        const uri = 'http://altkom.test/api/book';
        const obj = {
            isbn: isbn,
            title: title,
            author: author,
            type: type,
            pages: pages,
            release_date: release_date
        };
        this.http.post(uri, obj)
            .subscribe(res => console.log('Done'));
    }
    editBook(id,isbn,title,author,type,pages,release_date) {
        const uri = 'http://altkom.test/api/book/' + id;
        const obj = {
            isbn: isbn,
            title: title,
            author: author,
            type: type,
            pages: pages,
            release_date: release_date
        };
        this
            .http
            .put(uri, obj)
            .subscribe(res => console.log('Done'));
    }
    deleteBook(id) {
        const uri = 'http://altkom.test/api/book/' + id;
        return this
            .http
            .delete(uri)
            .map(res => {
                return res;
            });
    }
}
