import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {HttpClient} from '@angular/common/http';
import {BookService} from '../book.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent  {
    bsModalRef: BsModalRef;
    @Input()
    id: string;
    constructor(private modalService: BsModalService) {}

    openModalWithComponent(id) {

        const initialState = {
            id: this.id
        };
        this.bsModalRef = this.modalService.show(ModalContentComponent, {initialState});
        this.bsModalRef.content.closeBtnName = 'Close';
    }

}

/* This is a component which we pass in modal*/

@Component({
    selector: 'modal-content',
    template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left">Szczegóły </h4>
      <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <ul *ngFor="let item of book">
        <li>Data dodania do bazy: {{item.created_at}}</li>
        <li>ISBN: {{item.isbn}}</li>
          <li>Tytuł: {{item.title}}</li>
          <li>Autor: {{item.author}}</li>
          <li>Gatunki(string): {{item.type}}</li>
          <li>Gatunki(foreign key):
              <ng-container *ngFor="let type of item.types">
                  {{ type.name }},
              </ng-container>
          </li>
          <li>Liczba stron: {{item.pages}}</li>
          <li>Data wydania: {{item.release_date}}</li>
      </ul>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">{{closeBtnName}}</button>
    </div>
  `
})

export class ModalContentComponent implements OnInit {
    closeBtnName: string;
    id: number;
    book: any;

    constructor(public bsModalRef: BsModalRef,private http: HttpClient, private service: BookService) {}

    ngOnInit() {
        this.service.showBook(this.id).subscribe(res => {
            console.log(res);
            this.book = res;
        });
    }
}