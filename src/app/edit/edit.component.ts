import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {HttpClient} from '@angular/common/http';
import {BookService} from '../book.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent {

    bsModalRef: BsModalRef;
    angForm: FormGroup;

    @Input()
    id: string;
    constructor(private modalService: BsModalService) {}

    openModalWithComponent(id) {

        const initialState = {
            id: this.id
        };
        this.bsModalRef = this.modalService.show(EditContent, {initialState});
        this.bsModalRef.content.closeBtnName = 'Close';
    }

}


/* This is a component which we pass in modal*/

@Component({
    selector: 'modal-content',
    template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left">Szczegóły </h4>
      <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
        <form [formGroup]="angForm" novalidate>
            <div class="form-group">
                <label class="col-md-4">ISBN</label>
                <input type="text" class="form-control" formControlName="isbn" #isbn />
            </div>
            <div *ngIf="angForm.controls['isbn'].invalid && (angForm.controls['isbn'].dirty || angForm.controls['isbn'].touched)" class="alert alert-danger">
                <div *ngIf="angForm.controls['isbn'].errors.required">
                    ISBN is required.
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4">Tytuł</label>
                <input type="text" class="form-control" formControlName="title" #title/>
            </div>
            <div *ngIf="angForm.controls['title'].invalid && (angForm.controls['title'].dirty || angForm.controls['title'].touched)" class="alert alert-danger">
                <div *ngIf="angForm.controls['title'].errors.required">
                    Title is required.
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4">Autor</label>
                <input type="text" class="form-control" formControlName="author" #author/>
            </div>
            <div *ngIf="angForm.controls['author'].invalid && (angForm.controls['author'].dirty || angForm.controls['author'].touched)" class="alert alert-danger">
            </div>
            <div class="form-group">
                <label class="col-md-4">Typ</label>
                <input type="text" class="form-control" formControlName="type" #type/>
            </div>
            <div class="form-group">
                <label class="col-md-4">Strony</label>
                <input type="text" class="form-control" formControlName="pages" #pages/>
            </div>
            <div class="form-group">
                <label class="col-md-4">Data wydania</label>
                <input type="date" class="form-control" formControlName="release_date" #release_date/>
            </div>
            <div class="form-group">
                <button (click)="update(isbn.value, title.value, author.value, type.value, pages.value, release_date.value)" [disabled]="angForm.pristine || angForm.invalid" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">{{closeBtnName}}</button>
    </div>
  `
})

export class EditContent implements OnInit {
    closeBtnName: string;
    id: number;
    book: any;
    angForm: FormGroup;

    constructor(public bsModalRef: BsModalRef,private http: HttpClient, private service: BookService,
                private route: ActivatedRoute, private router: Router,  private fb: FormBuilder) {
        this.createForm();
    }
    createForm() {
        this.angForm = this.fb.group({
            isbn: ['', Validators.required ],
            title: ['', Validators.required ],
            author: ['' ],
            type: [''],
            pages: ['' ],
            release_date: ['', Validators.required ]
        });
    }
    update(isbn, title, author,type, pages, release_date) {
        this.service.editBook(this.id, isbn, title, author, type, pages, release_date);
    //    TODO: CLOSE AND UPDATE LIST
    }
        ngOnInit() {

            this.service.showBook(this.id).subscribe(res => {
                this.book = res[0];
                this.angForm.setValue({
                    title: this.book.title,
                    isbn: this.book.isbn,
                    author: this.book.author,
                    type: this.book.type,
                    pages: this.book.pages,
                    release_date: this.book.release_date,
                });
            });

        }
}