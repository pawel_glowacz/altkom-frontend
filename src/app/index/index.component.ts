import {BookService} from './../book.service';
import {Component, OnInit, TemplateRef} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
    books: any;
    modalRef: BsModalRef;
    angForm: FormGroup;

    constructor(private http: HttpClient, private service: BookService, private modalService: BsModalService,
                private fb: FormBuilder) {
        this.createForm();
    }


    createForm() {
        this.angForm = this.fb.group({
            isbn: ['', Validators.required],
            title: ['', Validators.required],
            author: [''],
            type: [''],
            pages: [''],
            release_date: ['']
        });
    }


    ngOnInit() {
        this.getBooks();
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    add(isbn, title, author, type, pages, release_date) {
        this.service.addBook(isbn, title, author, type, pages, release_date);
    //    TODO: PUSH TO ARRAY AND CLOSE MODAL
    }

    getBooks() {
        this.service.getBooks().subscribe(res => {
            this.books = res;
        });
    }

    deleteBook(id) {
        if (confirm('Are you sure to delete?')) {
            this.service.deleteBook(id).subscribe(res => {
                this.books.splice(this.books.indexOf(id), 1);
            });
        }
    }


}
