import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import {EditComponent, EditContent} from './edit/edit.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routerConfig';
import {HttpClientModule} from '@angular/common/http';
import { BookService } from './book.service';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import {ModalContentComponent, ShowComponent} from './show/show.component';


@NgModule({
  declarations: [
      AppComponent ,
      IndexComponent,
    CreateComponent,
    EditComponent,
    MenuComponent,
    HeaderComponent,
    ShowComponent,
      ModalContentComponent,
      EditContent
  ],
    entryComponents: [
        ShowComponent,
        ModalContentComponent,
        EditContent
    ],
  imports: [
    BrowserModule,RouterModule.forRoot(appRoutes),
      HttpClientModule,
      BsDropdownModule.forRoot(),
      TooltipModule.forRoot(),
      ModalModule.forRoot(),
      ReactiveFormsModule
  ],
    providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
